window.$ = window.jQuery = window.jquery = require('jquery');
require('onepage-scroll/jquery.onepage-scroll.min');
window.WOW = require('wowjs').WOW;



$(document).ready(function () {

    $(".spinner").textrotator({
        animation: "flipUp", // You can pick the way it animates when rotating through words. Options are dissolve (default), fade, flip, flipUp, flipCube, flipCubeUp and spin.
        separator: ",", // If you don't want commas to be the separator, you can define a new separator (|, &, * etc.) by yourself using this field.
        speed: 3000 // How many milliseconds until the next word show.
    });


    //Wow
    new WOW().init()

    if ($(window).width() < 800) {

        var controller = new ScrollMagic.Controller({globalSceneOptions: {duration: "100%"}});


        var SceneOne = new  ScrollMagic.Scene({
            triggerElement: '.screen-5'
        })
            .setClassToggle('.fixed-area', 'form-stage')
            .addTo(controller);

    }


    if ($(window).width() > 800) {

        // Scrollmagic

        var controller = new ScrollMagic.Controller({globalSceneOptions: {duration: "100%"}});


        var SceneOne = new  ScrollMagic.Scene({
            triggerElement: '.screen-5'
        })
            .setClassToggle('.fixed-area', 'form-stage')
            .addTo(controller);


        var SceneOne = new  ScrollMagic.Scene({
            triggerElement: '.screen-2'
        })
            .setClassToggle('body', 'sc-2')
            .addTo(controller);

        var SceneOne = new  ScrollMagic.Scene({
            triggerElement: '.screen-3'
        })
            .setClassToggle('body', 'sc-3')
            .addTo(controller);

        var SceneOne = new  ScrollMagic.Scene({
            triggerElement: '.screen-4'
        })
            .setClassToggle('body', 'sc-4')
            .addTo(controller);

        var SceneOne = new  ScrollMagic.Scene({
            triggerElement: '.screen-5'
        })
            .setClassToggle('body', 'sc-5')
            .addTo(controller);


        $('.section').each(function () {
            var SceneOne = new  ScrollMagic.Scene({
                triggerElement: this
            })
                .setClassToggle(this, 'test-stage')
                .addTo(controller);
        })


        var SceneOne = new  ScrollMagic.Scene({
            triggerElement: '.screen-5'
        })
            .setClassToggle('.form-block', 'test-stage-form')
            .addTo(controller);

    }


});


$(window).on('load', function() { // makes sure the whole site is loaded
    $('.preloader').delay(400).fadeOut(300); // will fade out the white DIV that covers the website.
    $('body').delay(350).css({'overflow':'visible'});
})




document.addEventListener("touchstart", function(){}, true);
